/* eslint-disable no-undef, @typescript-eslint/no-unused-vars, no-unused-vars */
import "./style.css";
import { Map_Style } from "./Defaults";
let newAddress: string = '';
export let address: string[] = [];
function addAddress(): void {
    let newAddress: HTMLInputElement = <HTMLInputElement>document.getElementById("newAddress");
    // address.push(newAddress);
    const checkboxArray = document.getElementById("waypoints") as HTMLSelectElement;
    checkboxArray.innerHTML += `<option value='${newAddress.value}'>${newAddress.value}</option>`;
    newAddress.value = '';

}
function initMap(): void {
    const polylineOptionsActual = {
        strokeColor: '#0096FF',
        strokeOpacity: 1.0,
        strokeWeight: 10
    };
    const directionsService = new google.maps.DirectionsService();
    const directionsRenderer = new google.maps.DirectionsRenderer({ polylineOptions: polylineOptionsActual });
    const map = new google.maps.Map(document.getElementById("map") as HTMLElement, {
        zoom: 13,
        center: { lat: 40.6780053, lng: -74.0015611 },
        styles: Map_Style,
        mapTypeId: 'roadmap'
    });

    directionsRenderer.setMap(map);

    (document.getElementById("submit") as HTMLElement).addEventListener("click", () => {
        calculateAndDisplayRoute(directionsService, directionsRenderer);
    });

    (document.getElementById("addAdd") as HTMLElement).addEventListener("click", () => {
        addAddress();
    });
}

function calculateAndDisplayRoute(directionsService: google.maps.DirectionsService, directionsRenderer: google.maps.DirectionsRenderer) {
    const waypts: google.maps.DirectionsWaypoint[] = [];
    const checkboxArray = document.getElementById("waypoints") as HTMLSelectElement;

    for (let i = 0; i < checkboxArray.length; i++) {
        if (checkboxArray.options[i].selected) {
            waypts.push({
                location: (checkboxArray[i] as HTMLOptionElement).value,
                stopover: true,
            });
        }
    }
    let method = (document.getElementById("method") as HTMLInputElement).value;
    directionsService
        .route({
            origin: (document.getElementById("start") as HTMLInputElement).value,
            destination: (document.getElementById("end") as HTMLInputElement).value,
            waypoints: waypts,
            optimizeWaypoints: true,
            travelMode: ((null != method) && (method === 'bicycle')) ? google.maps.TravelMode.BICYCLING : google.maps.TravelMode.WALKING,
        })
        .then((response) => {
            directionsRenderer.setDirections(response);

            const route = response.routes[0];
            const summaryPanel = document.getElementById("directions-panel") as HTMLElement;

            summaryPanel.innerHTML = "";

            // For each route, display summary information.
            for (let i = 0; i < route.legs.length; i++) {
                const routeSegment = i + 1;

                summaryPanel.innerHTML += "<b>Route Step: " + routeSegment + "</b><br>";
                summaryPanel.innerHTML += route.legs[i].end_address + "<br>";
            }
        })
        .catch((e) => window.alert("Directions request failed due to " + status));
}
export { initMap };
